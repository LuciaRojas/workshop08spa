## Workshop 08 - POC de SPA

Primero se crea el proyecto

`composer global require laravel/installer`

`composer create-project --prefer-dist laravel/laravel my_porject`

![coamdo tree](img/1.png "Archivo de Imagen")

Luego en el archivo llamado .env vamos a  conectarnos con la base de datos, necesitamos 
![coamdo tree](img/2.png "Archivo de Imagen")


SE crea el modelo de figuras , la migración y el controlador. 

`php artisan make:model figuras -mcr`


Luego en el archivo de migración create_figuras_table.php, vamos a poner lo siguiente

![coamdo tree](img/3.png "Archivo de Imagen")

Luego vamos a migrar la base de datos usando el siguiente comando:

`php artisan migrate`

Luego en el modelo figure.pvamos hacer lo siguiente:
![coamdo tree](img/4.png "Archivo de Imagen")


Luego vamos hacer  índice, agregar, editar, eliminar métodos en el archivo figureController:

![coamdo tree](img/5.png "Archivo de Imagen")
![coamdo tree](img/6.png "Archivo de Imagen")


 Ahora tenemos que ir a definir las rutas web y API. vamor a ir a  `web.php` 
 ![coamdo tree](img/7.png "Archivo de Imagen")


Luego en  api.php y vamos hacer lo siguiente:
 ![coamdo tree](img/8.png "Archivo de Imagen")

 Para presentar datos de forma declarativa al DOM usando Vue.js, necesitamos declarar la aplicación Vue.
 ![coamdo tree](img/9.png "Archivo de Imagen")

 Se agregaron 4 componentes de Vue.

App.vue
AllFigure.vue
AddFigure.vue
EditFigure.vue